//<editor-fold desc="Preamble">
/*
 * SSHJ PasswordChange example
 * Copyright (C) 2018  david.hubbard@infracentric.com

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//</editor-fold>
package sscce.sshj.passwordchange;

import com.hierynomus.sshj.signature.SignatureEdDSA;
import java.util.ArrayList;
import java.util.List;
import net.schmizz.sshj.DefaultConfig;
import net.schmizz.sshj.common.Factory;
import net.schmizz.sshj.signature.Signature;
import net.schmizz.sshj.signature.SignatureDSA;
import net.schmizz.sshj.signature.SignatureECDSA;
import net.schmizz.sshj.signature.SignatureRSA;

public class MySSHConfigImpl extends DefaultConfig {

    public MySSHConfigImpl() {
    }
    
    @Override
    public List<Factory.Named<Signature>> getSignatureFactories() {
        
        List<Factory.Named<Signature>> signatureFactories = new ArrayList<>();
        
        signatureFactories.add(new SignatureEdDSA.Factory());
        signatureFactories.add(new SignatureECDSA.Factory256());
        signatureFactories.add(new SignatureRSA.Factory());
        signatureFactories.add(new SignatureDSA.Factory());
        signatureFactories.add(new SignatureECDSA.Factory384());
        signatureFactories.add(new SignatureECDSA.Factory521());

        setSignatureFactories(signatureFactories);
        
        return super.getSignatureFactories();
    }
    
    
    
}
