//<editor-fold desc="Preamble">
/*
 * SSHJ PasswordChange example
 * Copyright (C) 2018  david.hubbard@infracentric.com

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//</editor-fold>
package sscce.sshj.passwordchange;

import sscce.sshj.passwordchange.handler.PasswordChangeHandler;
import sscce.sshj.passwordchange.handler.PasswordStage;
import java.io.IOException;
import java.security.PublicKey;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.transport.verification.HostKeyVerifier;
import net.schmizz.sshj.userauth.method.AuthMethod;
import net.schmizz.sshj.userauth.method.AuthPassword;
import net.sf.expectit.Expect;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import sscce.sshj.passwordchange.handler.PasswordChangeException;
import sscce.sshj.passwordchange.handler.PasswordStageMapLinux;

/**
 *
 * @author david
 */
public class MySSHPasswordTest {

    private Scanner s = new Scanner(System.in);
    
    public void run() {
        
        String host = "";
        String portVal = "";
        int port = 0;
        
        String username;
        char[] password;
        
        PasswordChangeHandler pwch = null;
        
        try {
            host = readLine("Enter Host: ");
            portVal = readLine("Enter Port Number: ");           
            if (!portVal.isEmpty()) {
                try {
                    port = Integer.parseInt(portVal);
                }
                catch (NumberFormatException ex) {
                    System.out.println("Invalid port entered ["+portVal+"] using 22");
                    port = 22;
                }
            }
            else {
                port = 22;
            }
            
            System.out.println("Connecting...");
            
            SSHClient ssh = new SSHClient(new MySSHConfigImpl());

            // promiscuous
            ssh.addHostKeyVerifier(
                new HostKeyVerifier() {
                    @Override
                    public boolean verify(String string, int i, PublicKey pk) {  
                        return true;                        
                    }

                });
        
            ssh.connect(host, port);            
            ssh.getConnection().getKeepAlive().setKeepAliveInterval(30);
        
            System.out.println("Connected");
            
            username = readLine("Enter Username: ");
            password = readLine("Enter Password (no masking): ").toCharArray();
            
            Security.addProvider(new BouncyCastleProvider());
            
            List<AuthMethod> authMethods = new ArrayList<>();

            MyPasswordFinder passwordFinder = new MyPasswordFinder(password);
            MyPasswordUpdateProvider newPasswordUpdateProvider = new MyPasswordUpdateProvider();
            AuthPassword ap = new AuthPassword(passwordFinder, newPasswordUpdateProvider);

            authMethods.add(ap);

            ssh.auth(username, authMethods);
            
            System.out.println("Got connected no exception");
            
            // if we got this far then we are connected - perform checking for password expiry
            pwch = new PasswordChangeHandler(ssh, new PasswordStageMapLinux(), password) {
                
                @Override
                public String handlePasswordEntryRequired(PasswordStage pChange, String triggerLine) {
                    return readLine(triggerLine+"Enter password ["+pChange.getStatus()+"] no masking)");
                }
            };

            PasswordStage pwStage = null;

            while (!pwch.isDone()) {

                pwStage = pwch.progressPasswordStage();

            }
            String pause = readLine("Success ("+(pwStage != null ? (pwStage.getStatus() == PasswordStage.STATUS_SUCCESS) : "is null")+") Hit Enter to continue ");

        } catch (IOException ex) {
            System.out.println("Got Exception ["+ex.getLocalizedMessage()+"]");
            ex.printStackTrace();
        } catch (PasswordChangeException ex) {
            System.out.println("Got Exception ["+ex.getLocalizedMessage()+"]");
            ex.printStackTrace();
        }
        finally {
            if (pwch != null) {
                pwch.close();
            }
        }
        
        System.exit(0);
    }
    
    public static void main(String[] args) {
        
        MySSHPasswordTest app = new MySSHPasswordTest();        
        app.run();
        
    }

    public String readLine(String prompt) {
        System.out.println(prompt);
        return s.nextLine();
    }

}
