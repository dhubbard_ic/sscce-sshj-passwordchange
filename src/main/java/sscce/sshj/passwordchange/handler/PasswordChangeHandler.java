//<editor-fold desc="Preamble">
/*
 * SSHJ PasswordChange example
 * Copyright (C) 2018  david.hubbard@infracentric.com

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//</editor-fold>
package sscce.sshj.passwordchange.handler;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.connection.ConnectionException;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.sf.expectit.Expect;
import net.sf.expectit.ExpectBuilder;
import net.sf.expectit.MultiResult;
import net.sf.expectit.Result;
import net.sf.expectit.matcher.Matcher;
import static net.sf.expectit.matcher.Matchers.anyOf;

/**
 *
 * @author david
 */
public abstract class PasswordChangeHandler {

    private PasswordStageMap passwordMap;
    private Matcher[] passwordStateMatches;

    private boolean done = false;
    
    private char[] currentPassword;
    
    SSHClient ssh;
    Session session = null;
    Session.Shell shell = null;
    Expect expect;

    // no current password, so user prompted for this
    public PasswordChangeHandler(SSHClient ssh, PasswordStageMap map) throws PasswordChangeException {
        this(ssh, map, null);    
    }
    
    // providing current password, so auto filled in
    public PasswordChangeHandler(SSHClient ssh, PasswordStageMap map, char[] currentPassword) throws PasswordChangeException {
        
        try {
            
            this.passwordMap = map;
            this.passwordStateMatches = map.getMatchers();
            
            this.currentPassword = currentPassword;
            this.ssh = ssh;
            
            session = ssh.startSession();
            session.allocateDefaultPTY();
            shell = session.startShell();

            expect = new ExpectBuilder()
                    .withOutput(shell.getOutputStream())
                    .withInputs(shell.getInputStream(), shell.getErrorStream())
                    .withExceptionOnFailure()
//                    .withEchoInput(System.out)
//                    .withEchoOutput(System.err)
                    .withTimeout(500, TimeUnit.MILLISECONDS)
                    .build();  
            
        } catch (IOException ex) {
            throw new PasswordChangeException("Unable to initialise PasswordChangeHandler", ex);
        }
            
    }
        
    public boolean isDone() {
        return done;
    }
    
    public PasswordStage progressPasswordStage() throws PasswordChangeException {
        
        PasswordStage pChange = null;
        
        try {

            MultiResult mres = expect.expect(anyOf(passwordStateMatches));                        

            // Check progress
            List<Result> results = mres.getResults();

            boolean gotMatch = true;
            
            for (int i = 0; i < results.size() || !gotMatch; i++) {

                gotMatch = false;
                
                Result result = results.get(i);

                if (result.isSuccessful()) {
                    gotMatch = true;
                    pChange = passwordMap.getPasswordStage(i);

                    // Note: a Multi-match may return more than one successful result
                    //       as "anyOf" is used. But we want to react to first one in this
                    //       pass and hence "break" is used to exit the for loop

                    if (pChange.atEnd()) {
                        result.canStopMatching();
                        break;
                    }

                    if (pChange.isInitial() && currentPassword != null) {
                        expect.sendLine(new String(currentPassword));
                        result.canStopMatching();
                        break;
                    }

                    // pass on result and and get next input                    
                    String newpassword2 = handlePasswordEntryRequired(pChange, pChange.getResultString(result));
                    
                    // send next password entry
                    try {
                        expect.sendLine(newpassword2);
                    }
                    catch (ConnectionException e) {                        
                        // expect this if we hit max retries - as ssh sesson gets killed by server
                    }
                    break;
                }
            }
            
            // if no matches found 
            if (!gotMatch || pChange != null && pChange.atEnd()) {
                done = true;
            }
        }
        catch  (IOException ex) {
            throw new PasswordChangeException("Error processing password change matching", ex);
        }
        return pChange;
    }
    
    public abstract String handlePasswordEntryRequired(PasswordStage pChange, String triggerLine);

    public void close() {
        if (shell != null) {
            try {
                shell.close();
            } catch (IOException ex) {
                    // ignore
            }
        }
        if (session != null) {
            try {
                session.close();                    
            } catch (IOException ex) {
                // ignore
            }
        }
    }
    
}
