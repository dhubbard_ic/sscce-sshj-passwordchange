//<editor-fold desc="Preamble">
/*
 * SSHJ PasswordChange example
 * Copyright (C) 2018  david.hubbard@infracentric.com

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//</editor-fold>

package sscce.sshj.passwordchange.handler;

import java.util.Arrays;
import java.util.List;
import net.sf.expectit.matcher.Matcher;
import static net.sf.expectit.matcher.Matchers.contains;

/**
 *
 * @author david
 */
public class PasswordStageMapLinux implements PasswordStageMap {
    
    private final List<PasswordStage> pList  = Arrays.asList(
        new PasswordStage(PasswordStage.PROC_AT_END, PasswordStage.STATUS_SUCCESS, "all authentication tokens updated successfully"),
        new PasswordStage(PasswordStage.PROC_AT_END, PasswordStage.STATUS_FAIL, "exhausted maximum number of retries"),
        new PasswordStage(PasswordStage.PROC_AT_END, PasswordStage.STATUS_FAIL, "Password change failed"),
        
        new PasswordStage(PasswordStage.PROC_CONTINUE, PasswordStage.STATUS_INITIAL, "WARNING: Your password has expired"),
        new PasswordStage(PasswordStage.PROC_CONTINUE, PasswordStage.STATUS_INITIAL, "Password expired"),
        new PasswordStage(PasswordStage.PROC_CONTINUE, PasswordStage.STATUS_STAGE_1, "passwords do not match"),
        new PasswordStage(PasswordStage.PROC_CONTINUE, PasswordStage.STATUS_STAGE_1, "BAD PASSWORD:"),

        new PasswordStage(PasswordStage.PROC_CONTINUE, PasswordStage.STATUS_STAGE_1, "New password:"),
        new PasswordStage(PasswordStage.PROC_CONTINUE, PasswordStage.STATUS_STAGE_2, "Retype new password:"),
        
        new PasswordStage(PasswordStage.PROC_AT_END, PasswordStage.STATUS_SUCCESS, "$")        
    );
    

    @Override
    public final Matcher[] getMatchers() {
        
        Matcher[] matcher = new Matcher[pList.size()];
        
        int i = 0;
        for (PasswordStage pChange : pList) {
            matcher[i] = contains(pChange.getCheckString());
            i++;
        }
        return matcher;
    }
    
    @Override
    public final PasswordStage getPasswordStage(int id) {
        return pList.get(id);
    }
}
