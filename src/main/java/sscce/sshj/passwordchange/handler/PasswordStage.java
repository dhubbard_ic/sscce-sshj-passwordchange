//<editor-fold desc="Preamble">
/*
 * SSHJ PasswordChange example
 * Copyright (C) 2018  david.hubbard@infracentric.com

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//</editor-fold>
package sscce.sshj.passwordchange.handler;

import net.sf.expectit.Result;

/**
 *
 * @author david
 */
public class PasswordStage {
    
    public static final int PROC_CONTINUE = 0;
    public static final int PROC_AT_END = 1;
    
    public static final int STATUS_INITIAL = -1;
    public static final int STATUS_SUCCESS = 0;
    public static final int STATUS_STAGE_1 = 1;
    public static final int STATUS_STAGE_2 = 2;
    public static final int STATUS_FAIL = 3;

    private int process = PROC_CONTINUE;
    private int status = STATUS_INITIAL;
    private String checkString;

    public PasswordStage(int process, int status, String checkString) {
        this.process = process;
        this.status = status;
        this.checkString = checkString;
    }

    public int getProcess() {
        return process;
    }

    public String getCheckString() {
        return checkString;
    }

    public int getStatus() {
        return status;
    }
    
    public boolean atEnd() {
        return (getProcess() == PROC_AT_END);
    }
    
    public boolean isInitial() {
        return (getStatus() == STATUS_INITIAL);
    }

    public boolean success() {
        return (getStatus() == STATUS_SUCCESS);
    }

    public boolean fail() {
        return (getStatus() == STATUS_FAIL);
    }
    
    public String getResultString(Result result) {
        String input = result.getInput();
        String[] lines = input.split("\n");
        for (String line : lines) {
            if (line.contains(this.getCheckString())) {
                return line;
            }
        }
        return result.getInput();
    }
}
