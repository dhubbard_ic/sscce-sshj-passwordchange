//<editor-fold desc="Preamble">
/*
 * SSHJ PasswordChange example
 * Copyright (C) 2018  david.hubbard@infracentric.com

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//</editor-fold>

package sscce.sshj.passwordchange;

import java.util.Scanner;
import net.schmizz.sshj.userauth.password.PasswordUpdateProvider;
import net.schmizz.sshj.userauth.password.Resource;

public class MyPasswordUpdateProvider implements PasswordUpdateProvider  {
        
        Scanner s = new Scanner(System.in);
        
        public MyPasswordUpdateProvider() {
            System.out.println("MyPasswordChangeHandler loaded");
        }

        @Override
        public boolean shouldRetry(Resource<?> resource) {
            return true;
        }

        @Override
        public char[] provideNewPassword(Resource<?> rsrc, String string) {
            // note no validation here - just proving (or not) that it's called
            System.out.println("Please enter Updated password: ");
            String np = s.next();
            return np.toCharArray();
        }

    }
